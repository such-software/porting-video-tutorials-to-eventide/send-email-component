module SendEmail
  module Substitute
    def self.build
      Send.build.tap do |substitute_sender|
        sink = SendEmail::Send.register_telemetry_sink(substitute_sender)
        substitute_sender.sink = sink
      end
    end

    Error = Class.new(RuntimeError)

    class Send
      include SendEmail::Send

      attr_accessor :sink

      def one_email
        raise Error if sink.sent_records.length > 1

        first_record = sink.sent_records.first

        first_record&.data&.email
      end
    end
  end
end
