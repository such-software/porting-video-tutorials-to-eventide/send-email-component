module SendEmail
  class SendGrid
    include SendEmail::Send

    def self.configure(receiver)
      instance = build

      receiver.send_email = instance
    end
  end
end
