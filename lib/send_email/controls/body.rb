module SendEmail
  module Controls
    module Body
      def self.example
        Html.example
      end

      module Text
        def self.example
          "i need to move $12 US pounds stirling"
        end
      end

      module Html
        def self.example
          "<h1>i need to move $12 US pounds stirling</h1>"
        end
      end
    end
  end
end
