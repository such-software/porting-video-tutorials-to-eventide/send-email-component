module SendEmail
  module Controls
    module Email
      def self.example
        ::SendEmail::Email.new(
          Subject.example,
          Body::Html.example,
          Body::Text.example,
          EmailAddress::From.example,
          EmailAddress::To.example
        )
      end
    end
  end
end
