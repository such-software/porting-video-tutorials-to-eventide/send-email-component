module SendEmail
  module Controls
    module EmailAddress
      def self.example
        From.example
      end

      module From
        def self.example
          "from@example.com"
        end
      end

      module To
        def self.example
          "to@example.com"
        end
      end
    end
  end
end
