module SendEmail
  module Controls
    module Send
      def self.example
        Example.build
      end

      class Example
        include ::SendEmail::Send
      end
    end
  end
end
