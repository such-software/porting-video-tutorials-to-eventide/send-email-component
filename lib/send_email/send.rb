module SendEmail
  module Send
    def self.included(cls)
      cls.class_exec do
        include Dependency

        dependency :telemetry, ::Telemetry

        extend Build

        const_set :Substitute, Substitute
      end
    end

    def call(email)
      # puts "We totally send emails. Like real emails."

      telemetry.record(:sent, Telemetry::Data.new(email))

      # Returning telemetry.records's return value seems odd
      nil
    end

    module Build
      def build
        new.tap do |instance|
          # Since we're not actually sending emails, we don't worry about
          # configuring any provider-specifics around doing so.  If we were,
          # we'd probably add another dependnecy for those specifics,
          # similar to how Messaging::Write has the dependency for
          # message_writer.
          # instance.configure
          ::Telemetry.configure(instance)
        end
      end
    end

    def self.register_telemetry_sink(sender)
      sink = Telemetry.sink
      sender.telemetry.register sink

      sink
    end

    module Telemetry
      class Sink
        include ::Telemetry::Sink

        record :sent
      end

      Data = Struct.new :email

      def self.sink
        Sink.new
      end
    end
  end
end
