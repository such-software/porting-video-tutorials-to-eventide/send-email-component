require "eventide/postgres"
require "send_email"

require "send_email_component/messages/commands/send"
require "send_email_component/messages/events/sent"

require "send_email_component/transmission"
require "send_email_component/projection"
require "send_email_component/store"
require "send_email_component/system_sender_email_address"

require "send_email_component/handlers/commands"

require "send_email_component/consumers/commands"

require "send_email_component/start"
