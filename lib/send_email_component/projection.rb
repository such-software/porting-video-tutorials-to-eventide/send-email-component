module SendEmailComponent
  class Projection
    include EntityProjection
    include Messages::Events

    entity_name :transmission

    apply Sent do |sent|
      SetAttributes.(transmission, sent, copy: [
        { :email_id => :id },
        :to,
        :subject,
        :text,
        :html
      ])

      sent_time = Clock.parse(sent.sent_time)

      transmission.sent_time = sent_time
    end
  end
end
