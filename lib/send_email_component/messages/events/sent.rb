module SendEmailComponent
  module Messages
    module Events
      class Sent
        include Messaging::Message 

        attribute :email_id, String
        attribute :to, String
        attribute :subject, String
        attribute :text, String
        attribute :html, String
        attribute :sent_time, String
      end
    end
  end
end
