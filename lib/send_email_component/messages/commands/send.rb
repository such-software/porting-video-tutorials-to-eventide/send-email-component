module SendEmailComponent
  module Messages
    module Commands
      class Send
        include Messaging::Message 

        attribute :email_id, String
        attribute :to, String
        attribute :subject, String
        attribute :text, String
        attribute :html, String
      end
    end
  end
end
