module SendEmailComponent
  module Controls
    module Events
      module Sent
        def self.example
          sent = Messages::Events::Sent.build

          sent.email_id = ID.example
          sent.to = EmailAddress.example
          sent.subject = subject
          sent.text = text
          sent.html = html
          sent.sent_time = Time::Effective::example

          sent
        end

        def self.subject
          ::SendEmail::Controls::Subject.example
        end

        def self.text
          ::SendEmail::Controls::Body::Text.example
        end

        def self.html
          ::SendEmail::Controls::Body::Html.example
        end
      end
    end
  end
end
