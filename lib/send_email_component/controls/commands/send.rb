module SendEmailComponent
  module Controls
    module Commands
      module Send
        def self.example(email_id: nil)
          send = Messages::Commands::Send.build

          send.email_id = email_id || ID.example
          send.to = EmailAddress.example
          send.subject = subject
          send.text = text
          send.html = html

          send
        end

        def self.subject
          ::SendEmail::Controls::Subject.example
        end

        def self.text
          ::SendEmail::Controls::Body::Text.example
        end

        def self.html
          ::SendEmail::Controls::Body::Html.example
        end
      end
    end
  end
end
