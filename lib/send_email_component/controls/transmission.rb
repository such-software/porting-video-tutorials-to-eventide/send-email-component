module SendEmailComponent
  module Controls
    module Transmission
      def self.example
        Send.example
      end

      def self.id
        ID.example(increment: id_increment)
      end

      def self.id_increment
        1111
      end

      module New
        def self.example
          SendEmailComponent::Transmission.build
        end
      end

      module Sent
        def self.example
          transmission = SendEmailComponent::Transmission.build

          transmission.id = Transmission.id
          transmission.sent_time = Time::Processed::Raw.example

          transmission
        end
      end
    end
  end
end
