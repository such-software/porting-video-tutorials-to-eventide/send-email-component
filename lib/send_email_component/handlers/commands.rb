module SendEmailComponent
  module Handlers
    class Commands
      include Messaging::Handle
      include Messaging::StreamName
      include Log::Dependency

      include Messages::Commands
      include Messages::Events

      dependency :write, Messaging::Postgres::Write
      dependency :clock, Clock::UTC
      dependency :store, Store
      dependency :send_email, SendEmail::SendGrid

      def configure
        Messaging::Postgres::Write.configure(self)
        Clock::UTC.configure(self)
        Store.configure(self)
        SendEmail::SendGrid.configure(self)
      end

      category :send_email

      handle Send do |send|
        email_id = send.email_id

        transmission, version = store.fetch(email_id, include: :version)

        if transmission.sent?
          logger.info(tag: :ignored) { "Command ignored (Command: #{send.message_type}, Email ID: #{email_id})" }
          return
        end

        email = ::SendEmail::Email.new(
          send.subject,
          send.html,
          send.text,
          SYSTEM_SENDER_EMAIL_ADDRESS,
          send.to
        )

        send_email.(email)

        sent = Sent.follow(send)

        sent.sent_time = clock.iso8601

        stream_name = stream_name(email_id)

        write.(sent, stream_name, expected_version: version)
      end
    end
  end
end
