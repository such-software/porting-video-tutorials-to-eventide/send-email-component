module SendEmailComponent
  class Transmission
    include Schema::DataStructure

    attribute :id, String
    attribute :to, String
    attribute :subject, String
    attribute :text, String
    attribute :html, String
    attribute :sent_time, Time

    def sent?
      !sent_time.nil?
    end
  end
end
