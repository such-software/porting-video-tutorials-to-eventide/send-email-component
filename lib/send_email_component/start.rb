# Component initiator user guide: http://docs.eventide-project.org/user-guide/component-host.html#component-initiator

module SendEmailComponent
  module Start
    def self.call
      Consumers::Commands.start("sendEmail:command")
    end
  end
end
