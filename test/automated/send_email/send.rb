require_relative "../automated_init"

context "SendEmail" do
  context "Send" do
    email = ::SendEmail::Controls::Email.example

    send = ::SendEmail::Controls::Send.example

    sink = ::SendEmail::Send.register_telemetry_sink(send)

    send.(email)

    test "Records written telemetry" do
      assert(sink.recorded_sent?)
    end

    context "Recorded Data" do
      data = sink.records[0].data

      test "email" do
        assert(data.email == email)
      end
    end
  end
end
