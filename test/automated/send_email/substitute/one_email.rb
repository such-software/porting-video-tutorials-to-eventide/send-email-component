require_relative "../../automated_init"

context "SendEmail" do
  context "Substitute" do
    context "one_message" do
      email = ::SendEmail::Controls::Email.example

      subject = email.subject or fail
      html = email.html or fail
      text = email.text or fail
      from = email.from or fail
      to = email.to or fail

      substitute = ::SendEmail::Substitute.build

      substitute.(email)

      sent_email = substitute.one_email

      test "one_message returned the email" do
        refute(sent_email.nil?)
      end

      context "attributes" do
        test "subject" do
          assert(sent_email.subject == subject)
        end

        test "html" do
          assert(sent_email.html == html)
        end

        test "text" do
          assert(sent_email.text == text)
        end

        test "from" do
          assert(sent_email.from == from)
        end

        test "to" do
          assert(sent_email.to == to)
        end
      end
    end
  end
end
