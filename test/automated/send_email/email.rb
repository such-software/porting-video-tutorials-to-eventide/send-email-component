require_relative '../automated_init'

context "SendEmail" do
  context "Email" do
    from = ::SendEmail::Controls::EmailAddress::From.example or fail
    to = ::SendEmail::Controls::EmailAddress::To.example or fail
    subject = ::SendEmail::Controls::Subject.example or fail
    text = ::SendEmail::Controls::Body::Text.example or fail
    html = ::SendEmail::Controls::Body::Html.example or fail

    email = ::SendEmail::Email.new(subject, html, text, from, to)

    test "from" do
      assert(email.from == from)
    end

    test "to" do
      assert(email.to == to)
    end

    test "subject" do
      assert(email.subject == subject)
    end

    test "text" do
      assert(email.text == text)
    end

    test "html" do
      assert(email.html == html)
    end
  end
end
