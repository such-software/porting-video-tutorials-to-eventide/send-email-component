require_relative '../automated_init'

context "Transmission" do
  context "sent?" do
    transmission = Controls::Transmission::Sent.example

    test "It thinks it's sent" do
      assert(transmission.sent?)
    end
  end
end
