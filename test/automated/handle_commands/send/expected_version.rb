require_relative '../../automated_init'

context "Handle commands" do
  context "Send" do
    context "Expected version" do
      handler = Handlers::Commands.new

      transmission = Controls::Transmission::New.example

      send = Controls::Commands::Send.example

      version = Controls::Version.example

      handler.store.add(send.email_id, transmission, version)

      handler.(send)

      writer = handler.write

      sent = writer.one_message do |event|
        event.instance_of?(Messages::Events::Sent)
      end

      test "Written with the entity's expected version" do
        written_to_stream = writer.written?(sent) do |_, expected_version|
          expected_version == version
        end

        assert(written_to_stream)
      end
    end
  end
end
