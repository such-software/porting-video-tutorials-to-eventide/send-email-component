require_relative '../../automated_init'

context "Handle commands" do
  context "Send" do
    context "Sent" do
      send = Controls::Commands::Send.example

      email_id = send.email_id or fail
      to = send.to or fail
      subject = send.subject or fail
      text = send.text or fail
      html = send.html or fail

      raw_sent_time = Controls::Time::Effective::Raw.example

      handler = Handlers::Commands.new

      handler.clock.now = raw_sent_time

      handler.(send)

      context "Email" do
        send_email = handler.send_email

        sent_email = send_email.one_email

        test "Email was sent" do
          refute(sent_email.nil?)
        end

        context "attributes" do
          test "subject" do
            assert(sent_email.subject == subject)
          end

          test "html" do
            assert(sent_email.html == html)
          end

          test "text" do
            assert(sent_email.text == text)
          end

          test "from" do
            assert(sent_email.from == SYSTEM_SENDER_EMAIL_ADDRESS)
          end

          test "to" do
            assert(sent_email.to == to)
          end
        end
      end

      context "Messaging" do
        writer = handler.write

        sent = writer.one_message do |event|
          event.instance_of?(Messages::Events::Sent)
        end

        test "Sent Event is Written" do
          refute(sent.nil?)
        end

        test "Written to the sendEmail stream" do
          written_to_stream = writer.written?(sent) do |stream_name|
            stream_name == "sendEmail-#{email_id}"
          end

          assert(written_to_stream)
        end

        context "Attributes" do
          test "email_id" do
            assert(sent.email_id == email_id)
          end

          test "to" do
            assert(sent.to == to)
          end

          test "subject" do
            assert(sent.subject == subject)
          end

          test "text" do
            assert(sent.text == text)
          end

          test "html" do
            assert(sent.html == html)
          end

          test "sent_time" do
            sent_time = Clock::UTC.iso8601(raw_sent_time)
            assert(sent.sent_time == sent_time)
          end
        end
      end
    end
  end
end
