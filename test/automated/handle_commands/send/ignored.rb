require_relative '../../automated_init'

context "Handle commands" do
  context "Send" do
    context "Ignored" do
      transmission = Controls::Transmission::Sent.example

      transmission_id = transmission.id or fail

      send = Controls::Commands::Send.example(email_id: transmission_id)

      handler = Handlers::Commands.new

      handler.store.add(transmission_id, transmission)

      handler.(send)

      send_email = handler.send_email
      sent_email = send_email.one_email

      test "Email was not sent" do
        assert(sent_email.nil?)
      end

      writer = handler.write

      sent = writer.one_message do |event|
        event.instance_of?(Messages::Events::Sent)
      end

      test "Sent Event is not written" do
        assert(sent.nil?)
      end
    end
  end
end
