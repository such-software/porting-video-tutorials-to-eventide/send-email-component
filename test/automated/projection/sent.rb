require_relative '../automated_init'

context "Projection" do
  context "Sent" do
    transmission = Controls::Transmission::New.example

    sent = Controls::Events::Sent.example

    email_id = sent.email_id or fail
    to = sent.to or fail
    subject = sent.subject or fail
    text = sent.text or fail
    html = sent.html or fail

    Projection.(transmission, sent)

    context "attributes" do
      test "id" do
        assert(transmission.id == email_id)
      end

      test "sent_time" do
        sent_time = Time.parse(sent.sent_time)
        assert(transmission.sent_time == sent_time)
      end

      test "to" do
        assert(transmission.to == to)
      end

      test "subject" do
        assert(transmission.subject == subject)
      end

      test "text" do
        assert(transmission.text == text)
      end

      test "html" do
        assert(transmission.html == html)
      end
    end
  end
end
